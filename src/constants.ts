export const breakpoints = { mobile: 0, desktop: 1200 };

export const formatNumber = (price: number): string =>
    new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(price);
