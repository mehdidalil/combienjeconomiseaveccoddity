import React, { ReactElement } from 'react';
import { formatNumber } from '../../../constants';
import { Important, ResultRow } from '../../atoms';
import { ESNType } from '../Content/Content';

import './Results.scss';

interface ResultsProps {
    esnType?: ESNType;
    prestaSize?: number;
}

interface StatsType {
    tjm: number;
    turnover: number;
    formationHours: number;
}

const stats: Record<ESNType, StatsType> = {
    big: {
        tjm: 500,
        turnover: 0.35,
        formationHours: 20,
    },
    medium: {
        tjm: 580,
        turnover: 0.25,
        formationHours: 45,
    },
};

const managerCost = 125;
const interviewDuration = 1.5;
const interviewPreparation = 4;
const interviewsNumber = 3;

const coddityStats: StatsType = {
    tjm: 475,
    turnover: 0.08,
    formationHours: 120,
};

const calcSaves = (esnType?: ESNType, prestaSize?: number) => {
    if (!esnType || !prestaSize) {
        return undefined;
    }

    const tjmSave = (stats[esnType].tjm - coddityStats.tjm) * 218 * prestaSize;
    const lessInterviews =
        Math.floor(prestaSize * (stats[esnType].turnover - coddityStats.turnover) * interviewsNumber) + 1;
    const workDaysSaved = Math.floor((lessInterviews * (interviewDuration + interviewPreparation)) / 8);
    const formationHoursGained = coddityStats.formationHours - stats[esnType].formationHours;
    const globalSaves = tjmSave + workDaysSaved * managerCost * 8;

    return {
        tjmSave,
        lessInterviews,
        workDaysSaved,
        formationHoursGained,
        globalSaves,
    };
};

const Results = ({ esnType, prestaSize }: ResultsProps): ReactElement => {
    const saves = calcSaves(esnType, prestaSize);

    return (
        <div className="results">
            <div className="header">
                <h1>Vous pourriez économiser:</h1>
                <div className="header-result">
                    <Important text={`${saves?.globalSaves ? formatNumber(saves?.globalSaves) : '...'} HT par an`} />{' '}
                    <span style={{ fontWeight: 'bold' }}>
                        avec <span className="coddity">C</span>oddity.
                    </span>
                </div>
            </div>
            <div className="list">
                <ResultRow
                    icon="/tjm.png"
                    message={
                        <>
                            <Important text={`${saves?.tjmSave ? formatNumber(saves?.tjmSave) : '...'} HT par an`} />{' '}
                            directement lié au TJM des consultants
                        </>
                    }
                />
                <ResultRow
                    icon="/entretien.png"
                    message={
                        <>
                            <Important text={`${saves?.lessInterviews || '...'} entretien(s) de recrutement par an`} />{' '}
                            en moins (soit <Important text={`${saves?.workDaysSaved || '...'} jours de travail`} /> )
                        </>
                    }
                />
                <ResultRow
                    icon="/formation.png"
                    message={
                        <>
                            <Important text={`${saves?.formationHoursGained || '...'}h de formation`} /> par an par
                            consultant
                        </>
                    }
                />
            </div>
            <div className="footer">
                <a href="https://calendly.com/louis-marslen/15min" className="normal-text">
                    Vous désirez en savoir plus ? Prenons quelques minutes pour en discuter !
                </a>
            </div>
        </div>
    );
};

export default Results;
