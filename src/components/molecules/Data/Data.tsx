import React, { ReactElement } from 'react';
import useBreakpoint from 'use-breakpoint';
import { breakpoints } from '../../../constants';
import { ESNTable, StatsTable } from '../../atoms';

import './Data.scss';

const Data = (): ReactElement => {
    const { minWidth, maxWidth, breakpoint } = useBreakpoint(breakpoints, 'desktop');

    const Precisions = (
        <div className="table-footer very-small-text gray-text">
            <sup>(1)</sup> Données estimées basées sur notre connaissance du marché, ou transmises par nos partenaires,
            approuvées par nos clients actuels.
            <br />
            <sup>(2)</sup> En accord avec le droit individuel à la formation (DIF) qui donne aux salariés du secteur
            privé en CDI un droit de 20 heures, chez <span className="coddity">C</span>oddity c'est environ 10h par mois
            donc 120h par an.
            <br />
            <sup>(3)</sup> Données calculées sur les 4 années d'existence de <span className="coddity">C</span>oddity.
        </div>
    );

    return (
        <div className="data">
            <div className="title-header">
                <h1>
                    <a name="donnees" />
                    Les <span className="blue-text">données moyennes</span> utilisées pour nos estimations.
                </h1>
            </div>
            <div className="data-container">
                <div className="left-part-container">
                    <StatsTable />
                    {breakpoint === 'mobile' && Precisions}
                </div>
                <div className="right-part-container">
                    <ESNTable />
                    {breakpoint === 'desktop' && Precisions}
                </div>
            </div>
        </div>
    );
};

export default Data;
