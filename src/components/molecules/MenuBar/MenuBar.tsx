import React, { ReactElement, useState } from 'react';
import useBreakpoint from 'use-breakpoint';
import { breakpoints } from '../../../constants';
import { MenuLogo } from '../../atoms';
import MenuNav from '../MenuNav/MenuNav';
import './MenuBar.scss';

const MenuBar = (): ReactElement => {
    const [open, isOpen] = useState(false);

    const { minWidth, maxWidth, breakpoint } = useBreakpoint(breakpoints, 'desktop');

    return (
        <>
            <div className="menu-bar">
                <div className="menu-bar-logo">
                    <MenuLogo />
                </div>
                {breakpoint === 'desktop' ? (
                    <div className="menu-bar-nav">
                        <MenuNav />
                    </div>
                ) : (
                    <div className="menu-bar-button">
                        <img src="/menu-icon.png" onClick={() => isOpen((open) => !open)} width={32} height={32} />
                    </div>
                )}
            </div>
            {breakpoint === 'mobile' && (
                <div className={`menu-bar-list ${open ? 'menu-bar-list-open' : 'menu-bar-list-close'}`}>
                    <MenuNav verticalList />
                </div>
            )}
        </>
    );
};

export default MenuBar;
