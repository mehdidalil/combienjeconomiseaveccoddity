import React, { ReactElement, useState } from 'react';
import Choose from '../Choose/Choose';
import Results from '../Results/Results';
import './Content.scss';

export type ESNType = 'big' | 'medium';

const Content = (): ReactElement => {
    const [esnType, setESNType] = useState<ESNType | undefined>('big');

    const [prestaSize, setPrestaSize] = useState<number | undefined>(25);

    return (
        <div className="content">
            <a name="calculer" />
            <div className="content-container">
                <div>
                    <div className="left-part-container">
                        <Choose
                            esnType={esnType}
                            prestaSize={prestaSize}
                            setESNType={setESNType}
                            setPrestaSize={setPrestaSize}
                        />
                    </div>
                </div>
                <div>
                    <div className="right-part-container">
                        <Results esnType={esnType} prestaSize={prestaSize} />
                        <div className="footer small-text">
                            Les chiffres avancés ne sont que des estimations. Les données utilisées pour les calculs
                            proviennent de notre connaissance du marché, détails ci-dessous.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Content;
