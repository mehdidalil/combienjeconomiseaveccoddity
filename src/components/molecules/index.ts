export { default as MenuBar } from './MenuBar/MenuBar';
export { default as Content } from './Content/Content';
export { default as Data } from './Data/Data';
export { default as Footer } from './Footer/Footer';
