import React, { ReactElement } from 'react';

import './Footer.scss';

const Footer = (): ReactElement => {
    return (
        <div className="footer" style={{ backgroundColor: 'white' }}>
            <div className="button-container">
                <button onClick={() => (location.href = 'https://calendly.com/louis-marslen/15min')}>
                    <h2>Curieux d'en savoir plus ? Rencontrons-nous !</h2>
                </button>
            </div>
            <div className="links gray-text very-small-text">
                <div className="copyright">
                    Copyright (c) CombienJEconomiseAvec<span className="coddity">C</span>oddity 2020
                </div>
                <div className="website">
                    <a href="https://www.coddity.com">https://www.coddity.com</a>
                </div>
            </div>
        </div>
    );
};

export default Footer;
