import React, { ReactElement } from 'react';
import { MenuAction } from '../../atoms';
import { MenuActionProps } from '../../atoms/MenuAction/MenuAction';

import './MenuNav.scss';

const MenuBarActions: MenuActionProps[] = [
    { label: 'Calculer', link: '/#calculer' },
    { label: 'Données', link: '/#donnees' },
    { label: 'Qui sommes nous ?', link: 'https://www.coddity.com', flex: 2 },
    { label: 'Rencontrons-nous !', link: 'https://calendly.com/louis-marslen/15min', type: 'button', flex: 2 },
];

interface MenuNavProps {
    verticalList?: boolean;
}

const MenuNav = ({ verticalList = false }: MenuNavProps): ReactElement => {
    return (
        <div className="menu-nav">
            {MenuBarActions.map(({ label, link, type, flex }) => (
                <MenuAction
                    key={link}
                    label={label}
                    link={link}
                    type={verticalList ? 'link' : type}
                    flex={verticalList ? 1 : flex}
                />
            ))}
        </div>
    );
};

export default MenuNav;
