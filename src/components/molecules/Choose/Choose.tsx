import React, { ReactElement } from 'react';
import { Select } from '../../atoms';
import { ESNType } from '../Content/Content';
import './Choose.scss';

interface ChooseProps {
    esnType?: ESNType;
    prestaSize?: number;
    setESNType: React.Dispatch<React.SetStateAction<ESNType | undefined>>;
    setPrestaSize: React.Dispatch<React.SetStateAction<number | undefined>>;
}

const Choose = ({ esnType, prestaSize, setESNType, setPrestaSize }: ChooseProps): ReactElement => {
    return (
        <div className="choose">
            <div className="header">
                <h1>
                    Découvrez combien vous pourriez économiser sur vos achats P2I en travaillant avec{' '}
                    <span className="coddity">C</span>oddity.
                </h1>
            </div>
            <div className="subheader">
                <h3 className="normal-text">
                    Renseignez les deux champs ci-dessous à propos des ESN référencées chez vous :
                </h3>
            </div>
            <div className="list">
                <Select
                    options={[
                        { label: 'ESN de taille importante', value: 'big' },
                        { label: 'ESN de taille intermédiaire', value: 'medium' },
                    ]}
                    placeholder="Type d'ESN référencée dans votre entreprise..."
                    value={esnType}
                    onChange={(value) => setESNType(value as ESNType)}
                />

                <Select
                    options={[
                        { label: '5 ETP', value: 5 },
                        { label: '10 ETP', value: 10 },
                        { label: '25 ETP', value: 25 },
                        { label: '50 ETP', value: 50 },
                        { label: '100 ETP', value: 100 },
                        { label: '250 ETP', value: 250 },
                    ]}
                    placeholder="Volume d'activité à sous-traiter..."
                    value={prestaSize}
                    onChange={(value) => {
                        setPrestaSize(value as number);
                    }}
                />
            </div>
        </div>
    );
};

export default Choose;
