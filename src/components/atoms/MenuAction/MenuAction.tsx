import React, { ReactElement } from 'react';
import './MenuAction.scss';

export interface MenuActionProps {
    label: string;
    link: string;
    type?: 'button' | 'link';
    flex?: number;
}

const MenuAction = ({ label = '', link = '', type = 'link', flex = 1 }: MenuActionProps): ReactElement | null => {
    if (type === 'link') {
        return (
            <div className="menu-action normal-text" style={{ flex }}>
                <a href={link}>{label}</a>
            </div>
        );
    } else if (type === 'button') {
        return (
            <div className="menu-action" style={{ flex }}>
                <button onClick={() => (location.href = link)} className="normal-text">
                    {label}
                </button>
            </div>
        );
    }
    return null;
};

export default MenuAction;
