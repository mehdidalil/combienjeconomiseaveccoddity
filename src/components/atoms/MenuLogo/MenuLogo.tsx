import React, { ReactElement } from 'react';
import './MenuLogo.scss';

const MenuLogo = (): ReactElement => {
    return (
        <div className="menu-logo">
            <div className="item item-logo">
                <img src="/logo coddity.png" />
            </div>
            <div className="item item-title">
                <h2>
                    <span>Combien </span>
                    <span>Jeconomise </span>
                    <span>Avec </span>
                    <span>
                        <b className="coddity">C</b>oddity.com{' '}
                    </span>
                </h2>
            </div>
        </div>
    );
};

export default MenuLogo;
