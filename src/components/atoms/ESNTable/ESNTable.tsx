import React, { ReactElement } from 'react';

import './ESNTable.scss';

const headers = [
    <div key="empty" />,
    <div key="turnover">Turnover % annuel moyen</div>,
    <div key="tjm">TJM moyen pour le développement web ou data</div>,
    <div key="formation">Durée annuelle allouée à la formation de consultants</div>,
];

const rows = {
    big: [
        <div key="big-name" className="name">
            ESN de taille importante
        </div>,
        <div key="big-turnover">
            35%<sup>(1)</sup>
        </div>,
        <div key="big-tjm">
            500€ HT<sup>(1)</sup>
        </div>,
        <div key="big-formation">
            20h<sup>(2)</sup>
        </div>,
    ],
    medium: [
        <div key="medium-name" className="name">
            ESN de taille intermédiaire
        </div>,
        <div key="medium-turnover">
            25%<sup>(1)</sup>
        </div>,
        <div key="medium-tjm">
            580€ HT<sup>(1)</sup>
        </div>,
        <div key="medium-formation">
            45h<sup>(2)</sup>
        </div>,
    ],
    coddity: [
        <div key="coddity-name" className="name">
            <span className="coddity">C</span>oddity
        </div>,
        <div key="coddity-turnover">
            8%<sup>(3)</sup>
        </div>,
        <div key="coddity-tjm">
            475€ HT<sup>(3)</sup>
        </div>,
        <div key="coddity-formation">
            120h<sup>(3)</sup>
        </div>,
    ],
};

const ESNTable = (): ReactElement => {
    return (
        <table className="esn-table">
            <thead>
                <tr>
                    {headers.map((value, index) => (
                        <td key={`esn-header-${index}`}>{value}</td>
                    ))}
                </tr>
            </thead>
            <tbody>
                {Object.values(rows).map((row, index) => (
                    <tr key={`esn-row-${index}`}>
                        {row.map((cell, index) => (
                            <td key={`esn-cell-${index}`}>{cell}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default ESNTable;
