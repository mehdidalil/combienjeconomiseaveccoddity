import React, { ReactElement, useEffect, useRef, useState } from 'react';
import useBreakpoint from 'use-breakpoint';
import { breakpoints } from '../../../constants';
import './Select.scss';

interface OptionType {
    label: string;
    value: string | number;
}

interface SelectProps {
    options: OptionType[];
    placeholder: string;
    value?: string | number;
    onChange: (value: string | number) => void;
}

/*
const Select = ({ options = [], placeholder, value = '', onChange }: SelectProps): ReactElement => {
    return (
        <div className="select">
            <select
                className="small-text"
                value={value}
                onChange={(event) => {
                    onChange(event.target.value);
                }}
            >
                <option value="" disabled hidden>
                    {placeholder}
                </option>
                {options.map(({ value = '', label = '' }, index) => (
                    <option key={`select-option-value-${value}-${index}`} value={value}>
                        {label}
                    </option>
                ))}
            </select>
        </div>
    );
};
*/

const Select = ({ options = [], placeholder, value = '', onChange }: SelectProps): ReactElement => {
    const [isOpen, setIsOpen] = useState(false);

    const { minWidth, maxWidth, breakpoint } = useBreakpoint(breakpoints, 'desktop');

    const wrapperRef = useRef(null);

    useEffect(() => {
        const handleClick = (evt: MouseEvent) => {
            if (wrapperRef.current && !(wrapperRef.current as any).contains(evt.target)) {
                if (isOpen) {
                    setIsOpen(false);
                }
            }
        };

        document.addEventListener('mousedown', handleClick);
        return () => {
            document.removeEventListener('mousedown', handleClick);
        };
    }, [isOpen, setIsOpen]);

    return (
        <div
            className={`custom-select-wrapper ${breakpoint === 'desktop' ? 'normal-text' : 'small-text'}`}
            ref={wrapperRef}
        >
            <div className={`custom-select ${isOpen ? 'open' : ''}`}>
                <div className="custom-select__trigger" onClick={() => setIsOpen(!isOpen)}>
                    <span>{options.find((option) => option.value === value)?.label || placeholder}</span>
                    <div className="arrow" />
                </div>
                <div className="custom-options">
                    {options.map(({ value = '', label = '' }, index) => (
                        <span
                            className="custom-option"
                            data-value={value}
                            key={`select-option-value-${value}-${index}`}
                            onClick={() => {
                                onChange(value);
                                setIsOpen(false);
                            }}
                        >
                            {label}
                        </span>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Select;
