import React, { ReactElement, ReactNode } from 'react';
import './ResultRow.scss';

interface ResultRowProps {
    icon: string;
    message: ReactNode;
}

const ResultRow = ({ icon, message }: ResultRowProps): ReactElement => {
    return (
        <div className="result-row">
            <div className="img">
                <img src={icon} />
            </div>
            <span className="text normal-text">{message}</span>
        </div>
    );
};

export default ResultRow;
