import React, { ReactElement } from 'react';
import './Important.scss';

interface ImportantProps {
    text: string;
}

const Important = ({ text = '' }: ImportantProps): ReactElement => <span className="important">{text}</span>;

export default Important;
