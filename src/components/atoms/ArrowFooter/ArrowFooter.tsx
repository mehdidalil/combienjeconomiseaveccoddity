import React, { ReactElement } from 'react';

import './ArrowFooter.scss';

const ArrowFooter = (): ReactElement => {
    return (
        <div className="arrow-footer">
            <img src="./arrow-down.png" width={32} height={32} />
        </div>
    );
};

export default ArrowFooter;
