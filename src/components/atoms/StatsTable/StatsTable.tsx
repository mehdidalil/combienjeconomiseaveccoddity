import React, { ReactElement } from 'react';

import './StatsTable.scss';

const rows = {
    number: [
        "Nombre d'entretiens opérationnels préalables au démarrage d'une prestation",
        <div key="number">
            3<sup>(1)</sup>
        </div>,
    ],
    duration: [
        "Durée moyenne d'un entretien avec préparation en heures",
        <div key="duration">
            1.5h<sup>(1)</sup>
        </div>,
    ],
    time: [
        "Temps additionnel (échanges avec les ingénieurs d'affaires, sélection des CV, débrief)",
        <div key="time">
            4h<sup>(1)</sup>
        </div>,
    ],
    cost: [
        "Coût horaire estimé d'un manager d'équipe",
        <div key="cost">
            125€<sup>(1)</sup>
        </div>,
    ],
};

const StatsTable = (): ReactElement => {
    return (
        <table className="stats-table">
            <tbody>
                {Object.values(rows).map((row, index) => (
                    <tr key={`stats-row-${index}`}>
                        {row.map((cell, index) => (
                            <td key={`stats-cell-${index}`}>{cell}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default StatsTable;
