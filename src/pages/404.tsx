import React, { ReactElement } from 'react';

const Page404 = (): ReactElement => <div style={{ backgroundColor: 'white' }}>Nothing to see here!</div>;

export default Page404;
