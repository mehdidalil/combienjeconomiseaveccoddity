import React, { ReactElement } from 'react';
import useBreakpoint from 'use-breakpoint';
import { ArrowFooter, Content, Data, Footer, MenuBar } from '../components';
import { breakpoints } from '../constants';
import { Helmet } from 'react-helmet';

import './index.css';

const App = (): ReactElement => {
    const { minWidth, maxWidth, breakpoint } = useBreakpoint(breakpoints, 'desktop');

    return (
        <>
            <Helmet>
                <title>CombienJeconomiseAvecCoddity</title>
            </Helmet>
            <MenuBar />
            <Content />
            {breakpoint === 'desktop' && <ArrowFooter />}
            <Data />
            <Footer />
        </>
    );
};

export default App;
